<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fulfillment extends Model
{

    const UNFULFILLED = 'UNFULFILLED';
    const FULFILLED   = 'FULFILLED';

    protected $table = 'fulfillments';    
    protected $fillable = ['account', 'api_id', 'order_api_id', 'line_items', 'tracking_url', 'tracking_number', 'tracking_company','status','last_sync'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_api_id', 'api_id');
    }   
}
