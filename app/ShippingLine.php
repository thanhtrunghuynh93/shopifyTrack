<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingLine extends Model
{
    protected $table = 'shipping_lines';    
    protected $fillable = ['order_api_id', 'account', 'name', 'address', 'phone', 'city', 'zip', 'province', 'country', 'latitude', 'longitude', 'country_code', 'province_code','last_sync'];
           
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_api_id', 'api_id');
    }    

}
