<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';    
    protected $fillable = ['shop_url', 'api_key', 'password'];

    public function getConfig(){

        $config = array(
            'ShopUrl' => $this->shop_url,
            'ApiKey' => $this->api_key,
            'Password' => $this->password,
        );

        return $config;
        

    }
    

}
