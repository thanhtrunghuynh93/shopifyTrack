<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariantGroup extends Model
{
    protected $table = 'product_variant_groups';    
    protected $fillable = ['account', 'name', 'variants', 'product_api_id'];
}
