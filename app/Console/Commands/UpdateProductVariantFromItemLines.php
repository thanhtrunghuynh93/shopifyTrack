<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Service\OrderService;

class UpdateProductVariantFromItemLines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:updateProducts {account_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update product variants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $account_id = $this->argument('account_id');
        
        echo("-----------Start update products for account ".$account_id." ----------- \n");
        \Log::info("-----------Start update products for account ".$account_id." -----------");
        
        $service = new OrderService($account_id);
        $service->updateProductVariants();
        
    }
}
