<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    protected $table = 'line_items';    
    protected $fillable = ['account', 'api_id', 'order_api_id', 'name', 'quantity', 'price', 'order_id', 'variant_api_id', 'product_api_id', 'created', 'last_sync'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_api_id', 'api_id');
    }
}
