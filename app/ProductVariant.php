<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\LineItem;


class ProductVariant extends Model
{
    protected $table = 'product_variants';    
    protected $fillable = ['account', 'name', 'number_of_units', 'product_variant_group', 'total_sold', 'total', 'price', 'variant_api_id', 'product_api_id', 'created', 'last_sync'];

    public static function updateProductVariantFromItemLines($account){

        $variants = LineItem::where('account', $account->id)
        ->groupBy('variant_api_id')
        ->selectRaw('sum(quantity) as total_sold, name, price, product_api_id, variant_api_id')->get();

        foreach($variants as $variant){
            if($variant['variant_api_id'] != ''){
                ProductVariant::updateOrCreate(
                    [
                        'variant_api_id'    =>  $variant['variant_api_id'],
                    ],
                    [
                        'account'           =>  $account->id,
                        'name'              =>  $variant['name'],
                        'total_sold'        =>  $variant['total_sold'],
                        'product_api_id'    =>  $variant['product_api_id'],
                        'price'             =>  $variant['price'],
                        'last_sync'         =>  date('Y-m-d H:i:s')
                    ]); 
            }
        }
        
        $productGroups = ProductVariantGroup::where('account', $account->id)->get();
        
        foreach($productGroups as $productGroup){
            $variant_ids = json_decode($productGroup->variants);
            $variants = [];
            $group_total_sold = 0;
            $group_total = 0;
            $group_price = 0;
            $group_product_ids = [];

            foreach($variant_ids as $variant_id){

                $variant = ProductVariant::where("variant_api_id", $variant_id)->first();
                array_push($variants, $variant);
                array_push($group_product_ids, $variant->product_api_id);
                $group_total_sold += $variant->total_sold;
                if($group_total != -1){
                    $group_total = is_null($variant->total)?-1:($group_total + $variant->total);
                }
                $group_price += $variant->price;
    
            }

            $group_price = $group_price / count($variants);
            
            foreach($variants as $variant){

                $variant->product_variant_group = $productGroup->id;
                $variant->update();
            }

            $productVariant = ProductVariant::where('variant_api_id', $productGroup->variants)->first();
            $productVariant->name = $productGroup->name;
            $productVariant->total_sold = $group_total_sold;
            $productVariant->total = ($group_total != -1)?$group_total: null;
            $productVariant->price = $group_price;
            $productVariant->product_api_id = json_encode($group_product_ids);
            $productVariant->last_sync = date('Y-m-d H:i:s');
            $productVariant->update();

            $productGroup->product_api_id = json_encode($group_product_ids);
            $productGroup->update();
            
        }
    }
}
