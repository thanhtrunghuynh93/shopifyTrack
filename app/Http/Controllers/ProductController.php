<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductVariant;
use App\ProductVariantGroup;
use Response;

class ProductController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $account = session('account');
        if($account == null){
            return redirect('account');
        }

        return view('product_variant.index');
    }

    public function editNumberOfUnits(Request $request){

        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['number_of_units']) || empty($data['variant_id'])){
                throw new \Exception("Invalid arguments");                        
            }            
    
            $variant = ProductVariant::updateOrCreate(
                [
                    'variant_api_id'           =>  $data['variant_id'],
                ],
                [
                    'number_of_units'          =>  $data['number_of_units']
                ]
            );

            return Response::json([
                'success'   => true,
                'data'      => $variant,
                'message'   => 'You modified successfully the variant'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }

    }

    public function edit(Request $request)
    {
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['total']) || empty($data['variant_id'])){
                throw new \Exception("Invalid arguments");                        
            }            
    
            $variant = ProductVariant::updateOrCreate(
                [
                    'variant_api_id'           =>  $data['variant_id'],
                ],
                [
                    'total'              =>  $data['total']
                ]
            );

            return Response::json([
                'success'   => true,
                'data'      => $variant,
                'message'   => 'You modified successfully the variant'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }

    public function read(Request $request)
    {
        $account = session('account');
        
        $columns = array( 
            0 => 'name', 
            1 => 'variant_api_id',
            2 => 'product_api_id',
            3 => 'number_of_units',
            4 => 'total_sold',
            5 => '(total_sold * number_of_units)',            
            6 => 'name'            
        );

        $search_by = $request->input('search_by');
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $totalData = ProductVariant::where('account', $account->id)->where('product_variant_group', 0)->count();
        
        if(empty($request->input('search.value'))){            
            $posts = ProductVariant::where('account', $account->id)
                    ->where('product_variant_group', 0)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();        
                        
            $totalFiltered = $totalData;                    
        }
        else {
            $search = $request->input('search.value'); 
            
            if($search_by === "line_items"){

                $posts = ProductVariant::where('account', $account->id)
                                        ->where('name', 'LIKE', "%{$search}%")
                                        ->where('product_variant_group', 0)
                                        ->offset($start)
                                        ->limit($limit)
                                        ->orderBy($order,$dir)
                                        ->get();            
                $totalFiltered = ProductVariant::where('account', $account->id)
                                                ->where('product_variant_group', 0)
                                                ->where('name', 'LIKE', "%{$search}%")
                                                ->count();            
            }
        }
        
        $data = array();
        if(!empty($posts)){
            $count = 0;
            foreach ($posts as $post){
                if(!is_numeric($post->variant_api_id)){
                    $variant_ids = json_decode($post->variant_api_id);
                    $product_ids = json_decode($post->product_api_id);
                    //Check if this variant is group of others variant
                    if(is_array($variant_ids)){

                        $variants = [];
                        $variant_id_html = '';
                        $name_html = '';
                        $number_of_units = '';
                        $number_sold_html ='';
                        $total_sold_html = '';
                        $number_unit_sold_html = '';
                        $number_unit_sold = 0;

                        foreach($variant_ids as $variant_id){            
                            $variant = ProductVariant::where("variant_api_id", $variant_id)->first();
                            array_push($variants, $variant); 
                            $variant_id_html .= $variant_id . "<br>";
                            $name_html .= $variant->name . "<br>"; 
                            $number_of_units .= '<form class="form-inline number_of_units">
                                                    <input class="" type="hidden" name="variant_id" value="'.$variant->variant_api_id.'">
                                                    <input class="form-control" type="number" name="number_of_units" value="'.$variant->number_of_units.'" style="width:80px;">
                                                    <div class="" name="message"></div>
                                                </form><br>';
                            $number_sold_html .= $variant->total_sold . "<br>";     
                            $number_unit_sold_html .= $variant->total_sold * $variant->number_of_units . "<br>";
                            $number_unit_sold += $variant->total_sold * $variant->number_of_units;
                            
                        }

                        $nestedData = [];
                        $nestedData['name'] = $name_html;
                        $nestedData['variant_id'] = $variant_id_html;
                        $nestedData['product_id'] = implode("<br>", $product_ids);
                        $nestedData['number_of_units'] = $number_of_units;    
                        $nestedData['number_sold'] = $number_sold_html . "Total: ".$post->total_sold;
                        $nestedData['number_unit_sold'] = $number_unit_sold_html . "Total: ". $number_unit_sold;
                        $nestedData['number_left'] = $post->total === null?'Not available':$post->total - $number_unit_sold;
                        
                        $options = '<form class="form-inline tracking">
                                        <input class="" type="hidden" name="variant_id" value=\''.$post->variant_api_id.'\'>
                                        <input class="form-control" type="number" name="total" value="'.$post->total.'" style="width:80px;">
                                        <div class="" name="message"></div>
                                    </form>';
                        $nestedData['option'] = $options;
    
                        $options = '<form class="form-inline grouping">
                                        <input class="" type="hidden" name="grouping_variant_id" value=\''.$post->variant_api_id.'\'>
                                        <input class="" type="hidden" name="grouping_product_id" value="'.$post->product_api_id.'">
                                        <input class="" type="hidden" name="grouping_variant_name" value="'. $name_html.'">
                                        <input class="form-control" onchange="checkbox(this)" type="checkbox" name="grouping">
                                        <div class="btn btn-info" name="group_button" onclick="groupVariant()" style="display:none;">Group?</div>
                                        <br><div class="btn btn-danger" name="degroup_button" onclick="degroupVariant(this)">Degroup?</div>
                                        <div class="" name="grouping_message"></div>
                                    </form>';
                        $nestedData['grouping'] = $options;
                        $data[] = $nestedData;

                    }

                }else{

                    $nestedData = [];
                    $nestedData['name'] = $post->name;
                    $nestedData['variant_id'] = $post->variant_api_id;
                    $nestedData['product_id'] = $post->product_api_id;    

                    $options = '<form class="form-inline number_of_units">
                                    <input class="" type="hidden" name="variant_id" value="'.$post->variant_api_id.'">
                                    <input class="form-control" type="number" name="number_of_units" value="'.$post->number_of_units.'" style="width:80px;">
                                    <div class="" name="message"></div>
                                </form>';
                    $nestedData['number_of_units'] = $options;    
                    $nestedData['number_sold'] = $post->total_sold;
                    $nestedData['number_unit_sold'] = $post->total_sold * $post->number_of_units;
                    $nestedData['number_left'] = $post->total === null?'Not available':$post->total - ($post->total_sold * $post->number_of_units);
                    
                    $options = '<form class="form-inline tracking">
                                    <input class="" type="hidden" name="variant_id" value="'.$post->variant_api_id.'">
                                    <input class="form-control" type="number" name="total" value="'.$post->total.'" style="width:80px;">
                                    <div class="" name="message"></div>
                                </form>';
                    $nestedData['option'] = $options;

                    $options = '<form class="form-inline grouping">
                                    <input class="" type="hidden" name="grouping_variant_id" value="'.$post->variant_api_id.'">
                                    <input class="" type="hidden" name="grouping_product_id" value="'.$post->product_api_id.'">
                                    <input class="" type="hidden" name="grouping_variant_name" value="'. $post->name.'">
                                    <input class="form-control" onchange="checkbox(this)" type="checkbox" name="grouping">
                                    <div class="btn btn-info" name="group_button" onclick="groupVariant()" style="display:none;">Group?</div>
                                    <div class="" name="grouping_message"></div>
                                </form>';
                    $nestedData['grouping'] = $options;
                    $data[] = $nestedData;
                }
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 

    }

    function groupVariant(Request $request){
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['group'])){
                throw new \Exception("Invalid arguments");                        
            }            

            $variant_ids = [];

            //Degroup all subgroup before merge all
            foreach($data['group'] as $variant_id){
                if(!is_numeric($variant_id)){
                    $temp_variant_ids = json_decode($variant_id);
                    //Check if this variant is group of others variant
                    if(is_array($temp_variant_ids)){
                        
                        $productGroup = ProductVariantGroup::where('variants', $variant_id)->first();
                        if($productGroup != null)
                            $productGroup->delete();
                        $productVariant = ProductVariant::where('variant_api_id', $variant_id)->first();
                        if($productVariant != null)
                            $productVariant->delete();
                        foreach($temp_variant_ids as $temp_variant_id){
                            array_push($variant_ids, $temp_variant_id);                            
                            $variant = ProductVariant::where("variant_api_id", $temp_variant_id)->first();
                            $variant->product_variant_group = 0;
                            $variant->update();
                        }

                    }
                }else{
                    array_push($variant_ids, $variant_id);
                }
            }

            $variant_names = [];
            $variants = [];
            $group_product_ids = [];
            $group_total_sold = 0;
            $group_total = 0;
            $group_price = 0;
            
            foreach($variant_ids as $variant_id){

                $variant = ProductVariant::where("variant_api_id", $variant_id)->first();
                array_push($variant_names, $variant->name);
                array_push($variants, $variant);
                array_push($group_product_ids, $variant->product_api_id);
                $group_total_sold += $variant->total_sold;
                if($group_total != -1){
                    $group_total = is_null($variant->total)?-1:($group_total + $variant->total);
                }
                $group_price += $variant->price;

            }

            //average price
            $group_price = $group_price / count($variants);
            
            $productGroup = ProductVariantGroup::create([
                'account'           =>     $account->id, 
                'name'              =>     implode("<br>", $variant_names),
                'variants'          =>     json_encode($variant_ids),
                'product_api_id'    =>     json_encode($group_product_ids),
            ]);

            foreach($variants as $variant){

                $variant->product_variant_group = $productGroup->id;
                $variant->update();
            }

            $productVariant = ProductVariant::create([
                'account'           =>     $account->id, 
                'name'              =>     $productGroup->name,
                'total_sold'        =>     $group_total_sold,
                'total'             =>     ($group_total != -1)?$group_total: null,
                'price'             =>     $group_price,
                'variant_api_id'    =>     json_encode($variant_ids),
                'product_api_id'    =>     json_encode($group_product_ids),
                'last_sync'         =>     date('Y-m-d H:i:s'),
            ]);
            
            return Response::json([
                'success'   => true,
                'data'      => $variant,
                'message'   => 'You modified successfully the variant group'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }

    function degroupVariant(Request $request){
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['variants_id'])){
                throw new \Exception("Invalid arguments");                        
            }            

            $variants_ids = json_decode($data['variants_id']);    
            $productGroup = ProductVariantGroup::where('variants', $data['variants_id'])->first();
            if($productGroup != null)
                $productGroup->delete();
            $productVariant = ProductVariant::where('variant_api_id', $data['variants_id'])->first();
            if($productVariant != null)
                $productVariant->delete();
            foreach($variants_ids as $variant_id){
                
                $variant = ProductVariant::where("variant_api_id", $variant_id)->first();
                $variant->product_variant_group = 0;
                $variant->update();
            }                            

            return Response::json([
                'success'   => true,
                'data'      => $productGroup,
                'message'   => 'You modified successfully the variant group'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }


    function longest_common_substring($words)
    {
      $words = array_map('strtolower', array_map('trim', $words));
      $sort_by_strlen = create_function('$a, $b', 'if (strlen($a) == strlen($b)) { return strcmp($a, $b); } return (strlen($a) < strlen($b)) ? -1 : 1;');
      usort($words, $sort_by_strlen);
      // We have to assume that each string has something in common with the first
      // string (post sort), we just need to figure out what the longest common
      // string is. If any string DOES NOT have something in common with the first
      // string, return false.
      $longest_common_substring = array();
      $shortest_string = str_split(array_shift($words));
      while (sizeof($shortest_string)) {
        array_unshift($longest_common_substring, '');
        foreach ($shortest_string as $ci => $char) {
          foreach ($words as $wi => $word) {
            if (!strstr($word, $longest_common_substring[0] . $char)) {
              // No match
              break 2;
            } // if
          } // foreach
          // we found the current char in each word, so add it to the first longest_common_substring element,
          // then start checking again using the next char as well
          $longest_common_substring[0].= $char;
        } // foreach
        // We've finished looping through the entire shortest_string.
        // Remove the first char and start all over. Do this until there are no more
        // chars to search on.
        array_shift($shortest_string);
      }
      // If we made it here then we've run through everything
      usort($longest_common_substring, $sort_by_strlen);
      return array_pop($longest_common_substring);
    }

}
