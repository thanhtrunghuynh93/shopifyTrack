<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Fulfillment;
use App\LineItem;
use App\TrackingUrls;
use App\Service\OrderService;
use DB;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = session('account');
        if($account == null){
            return redirect('account');
        }
        return view('order.index');
    }

    public function fetch($order_id)
    {
        $account = session('account');
        if($account == null){
            return redirect('account');
        }
        
        $service = new OrderService(session('account'));

        try{
            $order = $service->fetch($order_id);
            return json_encode($order);
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }


    public function read(Request $request)
    {
        $account = session('account');
        
        $columns = array( 
            0 => 'name', 
            1 => 'customer',
            2 => 'name',
            3 => 'total_price',
            4 => 'gateway',            
            5 => 'name'            
        );

        $search_by = $request->input('search_by');
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $totalData = Order::where('orders.account', $account->id)->count();
        
        if($search_by === "order_number"){
            
            $order_number_prefix = $request->input('order_number_prefix'); 
            $order_number_from = $request->input('order_number_from'); 
            $order_number_to = $request->input('order_number_to'); 

            //Normalize the number by adding '0' 
            if(strlen($order_number_to) > strlen($order_number_from)){
                for ($i=0; $i<(strlen($order_number_to) - strlen($order_number_from)); $i++){
                    $order_number_from = '0' . $order_number_from;
                }
            }
            if(empty($request->input('search.value'))){                 

                $posts = Order::where([
                                ['orders.account', '=', $account->id],
                                ['name', '>=', $order_number_prefix.$order_number_from],
                                ['name', '<=', $order_number_prefix.$order_number_to],
                            ])->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();            
                $totalFiltered = Order::where([
                                            ['orders.account', '=', $account->id],
                                            ['name', '>=', $order_number_prefix.$order_number_from],
                                            ['name', '<=', $order_number_prefix.$order_number_to],
                                        ])->count();            
                                    
            }else{
                $search = $request->input('search.value'); 

                $items = DB::raw("(SELECT DISTINCT line_items.order_api_id FROM line_items WHERE line_items.name LIKE '%{$search}%') as item");
                
                $posts = Order::where([
                    ['orders.account', '=', $account->id],
                    ['name', '>=', $order_number_prefix.$order_number_from],
                    ['name', '<=', $order_number_prefix.$order_number_to],
                ])->join($items, function($join){
                    $join->on("item.order_api_id","=","orders.api_id");
                })->offset($start)
                ->limit($limit)
                ->orderBy('orders.'.$order,$dir)
                ->select('orders.*')
                ->get();

                $totalFiltered = Order::where([
                                            ['orders.account', '=', $account->id],
                                            ['name', '>=', $order_number_prefix.$order_number_from],
                                            ['name', '<=', $order_number_prefix.$order_number_to],
                                        ])->join($items, function($join){
                                                $join->on("item.order_api_id","=","orders.api_id");
                                             })->count();
            }
        }else        
        if(empty($request->input('search.value'))){            
            $posts = Order::where('orders.account', $account->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();            
            $totalFiltered = $totalData;                    
        }else {
            
            $search = $request->input('search.value'); 

            if($search_by === "customer_name"){

                $customers = DB::raw("(SELECT DISTINCT customers.api_id FROM customers WHERE customers.name LIKE '%{$search}%') as customer");

                $posts =  Order::where('orders.account', $account->id)
                    ->join($customers, function($join){
                        $join->on("customer.api_id","=","orders.customer_api_id");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('orders.'.$order,$dir)
                    ->select('orders.*')
                    ->get();

                $totalFiltered = Order::where('orders.account', $account->id)
                    ->join('customers', 'customer_api_id', '=', 'customers.api_id')
                    ->where('customers.name','LIKE',"%{$search}%")
                    ->count();
            }

            if($search_by === "line_items"){

                $items = DB::raw("(SELECT DISTINCT line_items.order_api_id FROM line_items WHERE line_items.name LIKE '%{$search}%') as item");
                
                $posts = Order::where('orders.account', $account->id)
                ->join($items, function($join){
                    $join->on("item.order_api_id","=","orders.api_id");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('orders.'.$order,$dir)
                ->select('orders.*')
                ->get();

                $totalFiltered = Order::where('orders.account', $account->id)
                                      ->join($items, function($join){
                                                $join->on("item.order_api_id","=","orders.api_id");
                                             })->count();
                
            }
        }

        $tracking_urls = TrackingUrls::where('account', $account->id)->first();
        if($tracking_urls != null){
            $tracking_urls_options = explode("\n", $tracking_urls->urls);
        }else{
            $tracking_urls_options = [];
        }
        
        $data = array();
        if(!empty($posts)){
            $count = 0;
            foreach ($posts as $post){
                $nestedData = [];
                $nestedData['name'] = $post->name;
                $customerInfo = $post->customer === null?$post->customer_api_id:$post->customer->name ." (".$post->customer->email.")";
                $customerAddress = $post->shippingLine === null?"":"<br>".$post->shippingLine->address.", "
                                                                         .$post->shippingLine->province.", "
                                                                         .$post->shippingLine->city.", "
                                                                         .$post->shippingLine->country.", "
                                                                         ."ZIP: ".$post->shippingLine->zip;
                $nestedData['customer'] = $customerInfo . $customerAddress;
                //$nestedData['line_items'] = $post->getItemListName($post->lineItems);
                $nestedData['fulfillment'] = $post->getFulfillmentStatus($post->fulfillments);                
                $nestedData['total_price'] = $post->total_price;
                $nestedData['gateway'] = $post->gateway;
                $nestedData['created_at'] = $post->created;

                $options = "";
                foreach($post->lineItems as $item){
                    $fulfillment = Fulfillment::where('account', $account->id)
                                              ->where('order_api_id', $post->api_id)
                                              ->where('line_items', 'LIKE', "%{$item->api_id}%")
                                              ->first();      

                    if($fulfillment === null){
                        $options .= '<form class="form-inline tracking">
                                        <input class="" type="hidden" name="order_id" value="'.$post->api_id.'">
                                        <input class="" type="hidden" name="order_name" value="'.$post->name.'">
                                        <input class="" type="hidden" name="item_id" value="'.$item->api_id.'">
                                        <label>'.$item->name.'</label><br>
                                        <input class="form-control" name="tracking_number" placeholder="Tracking Number" style="width:120px;">
                                        <select class="form-control" name="tracking_url" placeholder="Tracking Url" style="width:120px;">';
                        $options .= '<option value="">----- Select an url -----</option>';
                        foreach($tracking_urls_options as $option){
                            $options .= '<option value="'.$option.'">'.$option.'</option>';    
                        }

                        $options .=     '</select>
                                        <input class="form-control" name="tracking_company" value="Other" placeholder="Tracking Company" style="width:120px;">
                                        <div class="" name="message"></div>
                                    </form>';
                    }else{
                        $options .= '<form class="form-inline tracking">
                                        <input class="" type="hidden" name="order_id" value="'.$post->api_id.'">
                                        <input class="" type="hidden" name="order_name" value="'.$post->name.'">
                                        <input class="" type="hidden" name="fulfillment_id" value="'.$fulfillment->id.'">
                                        <input class="" type="hidden" name="fulfillment_api_id" value="'.$fulfillment->api_id.'">
                                        <input class="" type="hidden" name="item_id" value="'.$item->api_id.'">
                                        <label>'.$item->name.'</label><br>
                                        <input class="" name="tracking_number" placeholder="Tracking Number" style="width:120px;" value="'.$fulfillment->tracking_number.'">
                                        <select class="form-control" name="tracking_url" placeholder="Tracking Url" style="width:120px;">';

                        $existed = false;
                        foreach($tracking_urls_options as $option){
                            if($fulfillment->tracking_url == $option){
                                $options .= '<option value="'.$option.'" selected>'.$option.'</option>';    
                                $existed = true;
                            }else{
                                $options .= '<option value="'.$option.'">'.$option.'</option>';    
                            }
                        }

                        if(!$existed){
                            $options .= '<option value="'.$fulfillment->tracking_url.'" selected>'.$fulfillment->tracking_url.'</option>';    
                        }


                        $options .= '</select>
                                     <input class="" name="tracking_company" placeholder="Tracking Company" style="width:120px;" value="'.$fulfillment->tracking_company.'">';
                        
                        if($fulfillment->status == 'success'){
                            $options .= '<div class="alert alert-success" name="message">Synchronized with shopify</div>';    
                        }else if($fulfillment->status == 'temp'){
                            $options .= '<div class="alert alert-warning" name="message">Waiting for verification</div>';    
                        }else{
                            $options .= '<div class="alert alert-danger" name="message">Error</div>';    
                        }
                        
                        $options .= '</form>';
                    }                         
                }

                $nestedData['option'] = $options;

                // if($post->getFulfillmentStatus($post->fulfillments) === Fulfillment::UNFULFILLED)
                //     $nestedData['option'] = '<button class="btn btn-primary" onclick="createFulfillment(\''.$post->api_id.'\');">Create Fulfillment</button>';
                // if($post->getFulfillmentStatus($post->fulfillments) === Fulfillment::FULFILLED)
                //     $nestedData['option'] = '<button class="btn btn-info" onclick="viewFulfillment(\''.$post->api_id.'\');">View Fulfillment</button>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
            );

        echo json_encode($json_data); 

    }
}
