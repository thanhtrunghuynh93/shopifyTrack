<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\OrderService;
use App\Account;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('account') === null)
            return redirect('account');
        return redirect('orders');
    }
}
