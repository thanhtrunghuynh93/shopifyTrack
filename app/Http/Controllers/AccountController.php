<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Account;
use Session;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::all();
        return view('auth.account', compact('accounts'));
    }

    public function select($id)
    {
        $account = Account::where('id', $id)->first();
        Session::put('account', $account);
        return redirect('');
    }
}
