<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Order;
use App\Service\FulfillmentService;
use App\TrackingUrls;
use App\Fulfillment;
use Auth;

class FulfillmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $account = session('account');
            $user = Auth::user();
                        
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['order_id']) || empty($data['tracking_number']) || 
               empty($data['tracking_url']) || empty($data['tracking_company'])){
                throw new \Exception("Invalid arguments");                        
            }            

            $order_id = $data['order_id'];
            $item_id = $data['item_id'];
            $tracking_number = $data['tracking_number'];
            $tracking_url = $data['tracking_url'];
            $tracking_company = $data['tracking_company'];
            
            if($user->role === "admin"){
                $service = new FulfillmentService($account);
                $fulfillment = $service->createFulfillment($order_id, $item_id, $tracking_number, $tracking_url, $tracking_company);
                Fulfillment::updateOrCreate(
                    [
                        'api_id'            =>  $fulfillment['id'],
                    ],
                    [
                        'account'           =>  $account->id,
                        'order_api_id'      =>  $order_id,
                        'line_items'        =>  json_encode(array((float)($item_id))),
                        'tracking_url'      =>  $fulfillment['tracking_url'],
                        'tracking_number'   =>  $fulfillment['tracking_number'],
                        'tracking_company'  =>  $fulfillment['tracking_company'],
                        'status'            =>  $fulfillment['status'],
                        'last_sync'         =>  date('Y-m-d H:i:s')
                    ]
                );    
            }else if($user->role === "trackingFiller"){
                $fulfillment = Fulfillment::create(
                    [
                        'account'           =>  $account->id,
                        'order_api_id'      =>  $order_id,
                        'line_items'        =>  json_encode(array((float)($item_id))),
                        'tracking_url'      =>  $tracking_url,
                        'tracking_number'   =>  $tracking_number,
                        'tracking_company'  =>  $tracking_company,
                        'status'            =>  'temp',
                        'last_sync'         =>  date('Y-m-d H:i:s')
                    ]
                );    
            }
            

            return Response::json([
                'success'   => true,
                'data'      => json_encode($fulfillment),        
                'message'   => 'You have created successfully the fulfillment'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }

    public function edit(Request $request)
    {
        try{
            $account = session('account');
            $user = Auth::user();
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['order_id']) || empty($data['fulfillment_id']) || empty($data['tracking_number']) || 
               empty($data['tracking_url']) || empty($data['tracking_company'])){
                throw new \Exception("Invalid arguments");                        
            }            

            $order_id = $data['order_id'];
            $fulfillment_id = $data['fulfillment_id'];
            $fulfillment_api_id = $data['fulfillment_api_id'];
            $item_id = $data['item_id'];
            $tracking_number = $data['tracking_number'];
            $tracking_url = $data['tracking_url'];
            $tracking_company = $data['tracking_company'];

            if($user->role === "admin"){
                
                $service = new FulfillmentService($account);

                if($fulfillment_api_id == NULL){
                    $shopifyFulfillment = $service->createFulfillment($order_id, $item_id, $tracking_number, $tracking_url, $tracking_company);
                    $fulfillment = Fulfillment::where('id', $fulfillment_id)->first();
                    $fulfillment->api_id = $shopifyFulfillment["id"];
                    $fulfillment->line_items = json_encode(array((float)$item_id));
                    $fulfillment->tracking_url = $tracking_url;
                    $fulfillment->tracking_number = $tracking_number;
                    $fulfillment->tracking_company = $tracking_company;
                    $fulfillment->status = $shopifyFulfillment['status'];
                    $fulfillment->last_sync = date('Y-m-d H:i:s');
                    $fulfillment->update();                    

                }else{
                    $fulfillment = $service->modifyFulfillment($order_id, $item_id, $fulfillment_api_id, $tracking_number, $tracking_url, $tracking_company);
                    Fulfillment::updateOrCreate(
                        [
                            'api_id'            =>  $fulfillment['id'],
                        ],
                        [
                            'account'           =>  $account->id,
                            'order_api_id'      =>  $order_id,
                            'line_items'        =>  json_encode(array((float)$item_id)),
                            'tracking_url'      =>  $fulfillment['tracking_url'],
                            'tracking_number'   =>  $fulfillment['tracking_number'],
                            'tracking_company'  =>  $fulfillment['tracking_company'],
                            'status'            =>  $fulfillment['status'],
                            'last_sync'         =>  date('Y-m-d H:i:s')
                        ]
                    );
                }                

                    
            }

            if($user->role === "trackingFiller"){
                $fulfillment = Fulfillment::where('id', $fulfillment_id)->first();

                if($fulfillment->line_items != json_encode(array((float)$item_id))||
                   $fulfillment->tracking_url != $tracking_url||
                   $fulfillment->tracking_number != $tracking_number||
                   $fulfillment->tracking_company != $tracking_company
                ){
                    $fulfillment->line_items = json_encode(array((float)$item_id));
                    $fulfillment->tracking_url = $tracking_url;
                    $fulfillment->tracking_number = $tracking_number;
                    $fulfillment->tracking_company = $tracking_company;
                    $fulfillment->status = 'temp';
                    $fulfillment->last_sync = date('Y-m-d H:i:s');
                    $fulfillment->update();
                }
            }

            return Response::json([
                'success'   => true,
                'data'      => json_encode($fulfillment),        
                'message'   => 'You have edited successfully the fulfillment'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }

    public function read(Request $request)
    {
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();
            $order = Order::where('api_id', $data['order_id'])->first();

            if($order == null){
                throw new \Exception("Order is not valid");    
            }

            $fulfillments = $order->fulfillments;
            foreach($fulfillments as $fulfillment){
                if($fulfillment->status == 'success'){
                    return Response::json([
                        'success'   => true,
                        'data'      => json_encode($fulfillment)        
                    ], 200);        
                }
            }

            throw new \Exception("There is no success fulfillment");    

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }

    }

    public function saveTrackingUrls(Request $request){
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $data = $request->all();

            if(empty($data['tracking_urls'])){
                throw new \Exception("Invalid arguments");                        
            }            
    
            $tracking_urls = TrackingUrls::updateOrCreate(
                [
                    'account'           =>  $account->id,
                ],
                [
                    'urls'              =>  $data['tracking_urls']
                ]
            );

            return Response::json([
                'success'   => true,
                'data'      => $tracking_urls,
                'message'   => 'You modified successfully the tracking urls'
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }

    public function trackingUrls(){
        try{
            $account = session('account');
            
            if($account == null){
                throw new \Exception("Session is not valid");    
            }

            $tracking_urls = TrackingUrls::where('account', $account->id)->first();
            if($tracking_urls === null){
                return Response::json([
                    'success'   => true,
                    'data'      => ''        
                ], 200);            
            }
            $urls = implode("\n", explode(",", $tracking_urls->urls));    

            return Response::json([
                'success'   => true,
                'data'      => $urls        
            ], 200);        

        }catch(\Exception $e){
            return Response::json([
                'success' => false,
                'message'=> $e->getMessage()
            ], 500);        
        }
    }


}
