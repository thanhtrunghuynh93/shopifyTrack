<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingUrls extends Model
{
    protected $table = 'tracking_urls';    
    protected $fillable = ['account', 'urls'];

    
}
