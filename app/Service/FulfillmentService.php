<?php

namespace App\Service;

use App\Account;
use App\Fulfillment;

class FulfillmentService
{
    private $shopify;
    private $account;

    public function __construct($account)
    {
        //Pass in account id
        if(!is_object($account)){
            $tmpAccount = Account::where('id', $account)->first();
            if($tmpAccount == null){
                echo("Wrong account information ". $account);
                return;
            }
             
            $this->account = $tmpAccount;                
        }else{
            $this->account = $account;                
        }
        
        $config = $this->account->getConfig();
        $this->shopify = new \PHPShopify\ShopifySDK($config);
                
    }

    // public function getFulfillment($order_id){
     
    //     $params = array(
    //         "tracking_number" => $trackingNumber,
    //         "tracking_urls" => [$tracking_url],
    //         "tracking_company" => "Other",
    //         "notify_customer" => false                        
    //     );

    //     try{
    //         $fulfillments = $shopify->Order($order_id)->Fulfillment()->get($params);
    //         if($fulfillments !== null){
    //             return $fulfillments[0];
    //         }

    //         return null;
            
    //     }catch(\Exception $e){
    //         echo($e->getMessage());
    //     }
        
    // }
            
    public function createFulfillment($order_id, $item_id, $tracking_number, $tracking_url, $tracking_company){

        // $tracking_numbers_array = explode(",", $tracking_numbers);
        // $tracking_urls_array = explode(",", $tracking_urls);

        $params = array(
            "tracking_number" => $tracking_number,
            "tracking_url" => $tracking_url,
            "tracking_company" => $tracking_company,
            "line_items" => [
                [
                  "id" => $item_id
                ]
            ],
            "notify_customer" => false                        
        );

        try{
            $fulfillment = $this->shopify->Order($order_id)->Fulfillment()->post($params);
            return $fulfillment;
        }catch(\Exception $e){
            echo($e->getMessage());
            return null;
        }

    }

    public function modifyFulfillment($order_id, $item_id, $fulfillment_id, $tracking_number, $tracking_url, $tracking_company){
        
        // $tracking_numbers_array = array_map('trim', explode(',', $tracking_numbers));
        // $tracking_urls_array = array_map('trim', explode(',', $tracking_urls));

        $params = array(
            "tracking_number" => $tracking_number,
            "tracking_url" => $tracking_url,
            "tracking_company" => $tracking_company,
            "line_items" => [
                [
                  "id" => $item_id
                ]
            ],
            "notify_customer" => false                        
        );

        try{
            $fulfillment = $this->shopify->Order($order_id)->Fulfillment($fulfillment_id)->put($params);
            return $fulfillment;

        }catch(\Exception $e){
            echo($e->getMessage());
        }

    }

    public function cancelFulfillment($order_id, $fulfillment_id){
        
        try{
            $fulfillment = $this->shopify->Order($order_id)->Fulfillment($fulfillment_id)->cancel();
        }catch(\Exception $e){
            echo($e->getMessage());
        }

    }

}
