<?php

namespace App\Service;

use App\Account;
use App\Customer;
use App\Order;
use App\LineItem;
use App\CrawlTask;
use App\Fulfillment;
use App\ProductVariant;
use App\ShippingLine;

class OrderService
{
    private $shopify;
    private $account;

    const TASK_TYPE_CRAWL_ALL = 'ORDER_CRAWL_ALL';
    const TASK_TYPE_CRAWL_NEW = 'ORDER_CRAWL_NEW';

    public function __construct($account)
    {
        //Pass in account id
        if(!is_object($account)){
            $tmpAccount = Account::where('id', $account)->first();
            if($tmpAccount == null){
                echo("Wrong account information ". $account);
                return;
            }
             
            $this->account = $tmpAccount;                
        }else{
            $this->account = $account;                
        }
        
        $config = $this->account->getConfig();
        $this->shopify = new \PHPShopify\ShopifySDK($config);
                
    }

    public function crawl($page = 1, $limit = 50, $status = "any", $fields = 'id, name, customer, total_price, line_items, gateway, fulfillments, created_at'){

        $params = array(
            'page'   => $page,
            'limit'  => $limit,
            "status" => $status,
            'fields' => $fields
        );

        try{
            $orders = $this->shopify->Order()->get($params);        
            return $orders;
        }catch(\Exception $e){
            echo($e->getMessage());
            return null;
        }       
    }

    public function fetch($order_api_id){
        $order = $this->shopify->Order($order_api_id)->get();        
        return $order; 
    }

    public function crawlAll(){

        $allOrders = [];
        $page = 1;
        $limit = 50;

        $currentRunningTask = CrawlTask::where('account', $this->account->id)
        ->where('type', self::TASK_TYPE_CRAWL_ALL)
        ->where('status', CrawlTask::RUNNING_STATUS)
        ->first();

        if($currentRunningTask !== null){
            echo("CrawlPlan.info::There is another task running for account ".$this->account->shop_url."\n");
            return;
        }

        $task = CrawlTask::create([
            'account'   =>  $this->account->id,
            'type'      =>  self::TASK_TYPE_CRAWL_ALL,
            'status'    =>  CrawlTask::RUNNING_STATUS,
        ]);    

        $count = 0;
        try{
            while(true){
                $orders = $this->crawl($page++, $limit);            
                if($orders == null){
                    break;
                }
                foreach($orders as $order){                    

                    var_dump($order);
                    
                    foreach($order['line_items'] as $item){

                        LineItem::updateOrCreate(
                            [
                                'api_id'            =>  $item['id'],
                            ],
                            [
                                'account'           =>  $this->account->id,
                                'order_api_id'      =>  $order['id'],
                                'name'              =>  $item['name'],
                                'quantity'          =>  $item['quantity'],
                                'price'             =>  $item['price'],
                                'variant_api_id'    =>  $item['variant_id'],
                                'product_api_id'    =>  $item['product_id'],
                                'last_sync'         =>  date('Y-m-d H:i:s')
                            ]
                        );
                    }

                    Customer::updateOrCreate(
                        [
                            'api_id'            =>  $order['customer']['id'],
                        ],
                        [
                            'account'           =>  $this->account->id,
                            'name'              =>  $order['customer']['first_name']." ".$order['customer']["last_name"],
                            'email'             =>  $order['customer']['email'],
                            'created'           =>  $order['customer']['created_at'],
                            'last_sync'         =>  date('Y-m-d H:i:s')
                        ]);

                        if(!empty($order['shipping_address'])){
                            ShippingLine::updateOrCreate(
                                [
                                    'order_api_id'      =>  $order['id'],
                                ],
                                [
                                    'account'           =>  $this->account->id,
                                    'name'              =>  $order['shipping_address']['name'],
                                    'address'           =>  $order['shipping_address']['address1'],
                                    'phone'             =>  $order['shipping_address']['phone'],
                                    'city'              =>  $order['shipping_address']['city'],
                                    'zip'               =>  $order['shipping_address']['zip'],
                                    'province'          =>  $order['shipping_address']['province'],
                                    'country'           =>  $order['shipping_address']['country'],
                                    'province_code'     =>  $order['shipping_address']['province_code'],
                                    'country_code'      =>  $order['shipping_address']['country_code'],                            
                                    'last_sync'         =>  date('Y-m-d H:i:s')
                                ]);
                        }else if(!empty($order['customer']['default_address'])){
                            ShippingLine::updateOrCreate(
                                [
                                    'order_api_id'      =>  $order['id'],
                                ],
                                [
                                    'account'           =>  $this->account->id,
                                    'name'              =>  $order['customer']['default_address']['name'],
                                    'address'           =>  $order['customer']['default_address']['address1'],
                                    'phone'             =>  $order['customer']['default_address']['phone'],
                                    'city'              =>  $order['customer']['default_address']['city'],
                                    'zip'               =>  $order['customer']['default_address']['zip'],
                                    'province'          =>  $order['customer']['default_address']['province'],
                                    'country'           =>  $order['customer']['default_address']['country'],
                                    'province_code'     =>  $order['customer']['default_address']['province_code'],
                                    'country_code'      =>  $order['customer']['default_address']['country_code'],                            
                                    'last_sync'         =>  date('Y-m-d H:i:s')
                                ]);

                        }

                    foreach($order['fulfillments'] as $fulfillment){

                        $line_items = [];
                        foreach($fulfillment["line_items"] as $item){
                            array_push($line_items, $item["id"]);                            
                        }

                        Fulfillment::updateOrCreate(
                            [
                                'api_id'            =>  $fulfillment['id'],
                            ],
                            [
                                'account'           =>  $this->account->id,
                                'order_api_id'      =>  $order['id'],
                                'line_items'        =>  json_encode($line_items),
                                'tracking_url'      =>  $fulfillment['tracking_url'],
                                'tracking_number'   =>  $fulfillment['tracking_number'],
                                'tracking_company'  =>  $fulfillment['tracking_company'],
                                'status'            =>  $fulfillment['status'],
                                'last_sync'         =>  date('Y-m-d H:i:s')
                            ]
                        );

                    }

                    Order::updateOrCreate(
                    [
                        'api_id'                =>  $order['id'],
                    ],
                    [
                        'account'           =>  $this->account->id,
                        'customer_api_id'   =>  $order['customer']['id'],
                        'name'              =>  $order['name'],
                        'total_price'       =>  $order['total_price'],
                        'gateway'           =>  $order['gateway'],
                        'created'           =>  $order['created_at'],
                        'last_sync'         =>  date('Y-m-d H:i:s')
                    ]); 

                    $count++;

                    echo("Crawled " . $count. " orders\n");   
                    $task->log = "Crawled " . $count. " orders";
                    $task->update();
                }
            }
        }catch(\Exception $e){
            $task->log = $e->getMessage();
            $task->status = CrawlTask::ERRORED_STATUS;
            $task->update();
            return;    
        }
        
        $outdate_objects = Order::where('account', $this->account->id)
        ->where('last_sync','<',$task->created_at)
        ->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }
        $outdate_objects = Customer::where('account', $this->account->id)->where('last_sync','<',$task->created_at)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }
        $outdate_objects = LineItem::where('account', $this->account->id)->where('last_sync','<',$task->created_at)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }

        $outdate_objects = Fulfillment::where('account', $this->account->id)->where('last_sync','<',$task->created_at)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }

        $outdate_objects = ShippingLine::where('account', $this->account->id)->where('last_sync','<',$task->created_at)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }

        //Update products
        ProductVariant::updateProductVariantFromItemLines($this->account);

        //Clear old task
        $lastweek = date('Y-m-d H:i:s', strtotime('-7 days'));
        $outdate_objects = CrawlTask::where('account', $this->account->id)->where('created_at','<',$lastweek)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }

        $outdate_objects = ProductVariant::where('account', $this->account->id)->where('last_sync','<',$task->created_at)->get();
        foreach($outdate_objects as $object){
            $object->delete();
        }

        $task->log = 'Crawled successfully ' . $count . ' new orders';
        $task->status = CrawlTask::FINISHED_STATUS;
        $task->update();
        return;
    }

    public function updateProductVariants(){
        ProductVariant::updateProductVariantFromItemLines($this->account);
    }

    public function crawlNew(){
        
        $allOrders = [];
        $page = 1;
        $limit = 50;
        
        $currentRunningTask = CrawlTask::where('account', $this->account->id)
                                       ->where('type', self::TASK_TYPE_CRAWL_NEW)
                                       ->where('status', CrawlTask::RUNNING_STATUS)
                                       ->first();
        
                if($currentRunningTask !== null){
                    echo("CrawlPlan.info::There is another task running for account ".$this->account->shop_url."\n");
                    return;
                }
        
                $task = CrawlTask::create([
                    'account'   =>  $this->account->id,
                    'type'      =>  self::TASK_TYPE_CRAWL_NEW,
                    'status'    =>  CrawlTask::RUNNING_STATUS,
                ]);    
        
                $count = 0;
                try{
                    while(true){
                        $orders = $this->crawl($page++, $limit);            
                        if($orders == null){
                            //Update products
                            ProductVariant::updateProductVariantFromItemLines($this->account);    
                                                    
                            $task->log = 'Crawled successfully ' . $count . ' new orders';
                            $task->status = CrawlTask::FINISHED_STATUS;
                            $task->update();
                            return;
                        }
                        foreach($orders as $order){                    
        
                            if(Order::where('api_id', $order['id'])->where('account', $this->account->id)->first() !== null){
                                //Update products
                                ProductVariant::updateProductVariantFromItemLines($this->account);

                                $task->log = 'Crawled successfully ' . $count . ' new orders';
                                $task->status = CrawlTask::FINISHED_STATUS;
                                $task->update();
                                return;
                            }                    
                            
                            foreach($order['line_items'] as $item){
        
                                LineItem::updateOrCreate(
                                    [
                                        'api_id'            =>  $item['id'],
                                    ],
                                    [
                                        'account'           =>  $this->account->id,
                                        'order_api_id'      =>  $order['id'],
                                        'name'              =>  $item['name'],
                                        'quantity'          =>  $item['quantity'],
                                        'price'             =>  $item['price'],
                                        'variant_api_id'    =>  $item['variant_id'],
                                        'product_api_id'    =>  $item['product_id'],
                                        'last_sync'         =>  date('Y-m-d H:i:s')
                                    ]
                                );
                            }
        
                            Customer::updateOrCreate(
                                [
                                    'api_id'            =>  $order['customer']['id'],
                                ],
                                [
                                    'account'           =>  $this->account->id,
                                    'name'              =>  $order['customer']['first_name']." ".$order['customer']["last_name"],
                                    'email'             =>  $order['customer']['email'],
                                    'created'           =>  $order['customer']['created_at'],
                                    'last_sync'         =>  date('Y-m-d H:i:s')
                                ]);

                            if(!empty($order['shipping_address'])){
                                ShippingLine::updateOrCreate(
                                    [
                                        'order_api_id'      =>  $order['id'],
                                    ],
                                    [
                                        'account'           =>  $this->account->id,
                                        'name'              =>  $order['shipping_address']['name'],
                                        'address'           =>  $order['shipping_address']['address1'],
                                        'phone'             =>  $order['shipping_address']['phone'],
                                        'city'              =>  $order['shipping_address']['city'],
                                        'zip'               =>  $order['shipping_address']['zip'],
                                        'province'          =>  $order['shipping_address']['province'],
                                        'country'           =>  $order['shipping_address']['country'],
                                        'province_code'     =>  $order['shipping_address']['province_code'],
                                        'country_code'      =>  $order['shipping_address']['country_code'],                            
                                        'last_sync'         =>  date('Y-m-d H:i:s')
                                    ]);
                            }else if(!empty($order['customer']['default_address'])){
                                ShippingLine::updateOrCreate(
                                    [
                                        'order_api_id'      =>  $order['id'],
                                    ],
                                    [
                                        'account'           =>  $this->account->id,
                                        'name'              =>  $order['customer']['default_address']['name'],
                                        'address'           =>  $order['customer']['default_address']['address1'],
                                        'phone'             =>  $order['customer']['default_address']['phone'],
                                        'city'              =>  $order['customer']['default_address']['city'],
                                        'zip'               =>  $order['customer']['default_address']['zip'],
                                        'province'          =>  $order['customer']['default_address']['province'],
                                        'country'           =>  $order['customer']['default_address']['country'],
                                        'province_code'     =>  $order['customer']['default_address']['province_code'],
                                        'country_code'      =>  $order['customer']['default_address']['country_code'],                            
                                        'last_sync'         =>  date('Y-m-d H:i:s')
                                    ]);

                            }
                            
                            foreach($order['fulfillments'] as $fulfillment){
        
                                $line_items = [];
                                foreach($fulfillment["line_items"] as $item){
                                    array_push($line_items, $item["id"]);                            
                                }
        
                                Fulfillment::updateOrCreate(
                                    [
                                        'api_id'            =>  $fulfillment['id'],
                                    ],
                                    [
                                        'account'           =>  $this->account->id,
                                        'order_api_id'      =>  $order['id'],
                                        'line_items'        =>  json_encode($line_items),
                                        'tracking_url'      =>  $fulfillment['tracking_url'],
                                        'tracking_number'   =>  $fulfillment['tracking_number'],
                                        'tracking_company'  =>  $fulfillment['tracking_company'],
                                        'status'            =>  $fulfillment['status'],
                                        'last_sync'         =>  date('Y-m-d H:i:s')
                                    ]
                                );
        
                            }
        
                            Order::updateOrCreate(
                            [
                                'api_id'                =>  $order['id'],
                            ],
                            [
                                'account'           =>  $this->account->id,
                                'customer_api_id'   =>  $order['customer']['id'],
                                'name'              =>  $order['name'],
                                'total_price'       =>  $order['total_price'],
                                'gateway'           =>  $order['gateway'],
                                'created'           =>  $order['created_at'],
                                'last_sync'         =>  date('Y-m-d H:i:s')
                            ]); 
        
                            $count++;
        
                            echo("Crawled " . $count. " orders\n");   
                            $task->log = "Crawled " . $count. " orders";
                            $task->update();
                        }
                    }
                    ProductVariant::updateProductVariantFromItemLines($this->account);
                }catch(\Exception $e){
                    $task->log = $e->getMessage();
                    $task->status = CrawlTask::ERRORED_STATUS;
                    $task->update();
                    return;    
                }
                
               
            }
}
