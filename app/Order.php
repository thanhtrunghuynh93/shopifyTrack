<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fulfillment;
use App\ShippingLine;

class Order extends Model
{
    protected $table = 'orders';    
    protected $fillable = ['account', 'api_id', 'customer_api_id', 'name', 'total_price', 'gateway', 'created', 'last_sync'];

    public function lineItems(){
        return $this->hasMany('App\LineItem','order_api_id', 'api_id');
    }

    public function fulfillments(){
        return $this->hasMany('App\Fulfillment','order_api_id','api_id');
    }

    public function shippingLine()
    {
        return $this->hasOne('App\ShippingLine','order_api_id','api_id');
    }
    
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_api_id', 'api_id');
    }    

    public function getFulfillmentStatus($fulfillments){

        if($fulfillments === []){
            return Fulfillment::UNFULFILLED;
        }

        foreach($fulfillments as $fulfillment){
            if($fulfillment->status == "success"){
                return Fulfillment::FULFILLED;
            }            
        }

        return Fulfillment::UNFULFILLED;

    }

    public function getItemListName($items){       
        
        $result = '';
        foreach($items as $item){
            $result .= $item->name . "<br>";
        }

        return $result;
        
    }
}
