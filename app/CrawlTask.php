<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrawlTask extends Model
{
    
    protected $table = 'crawl_tasks';    
    protected $fillable = ['account', 'type', 'status'];

    const RUNNING_STATUS = 'RUNNING';
    const FINISHED_STATUS = 'FINISHED';
    const ERRORED_STATUS = 'ERRORED';

}
