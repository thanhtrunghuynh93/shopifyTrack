<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';    
    protected $fillable = ['account', 'api_id', 'name', 'email', 'created', 'last_sync'];

    public function orders(){
        return $this->hasMany('App\Order', 'api_id', 'customer_api_id');
    }
}
