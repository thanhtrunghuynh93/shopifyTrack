<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFulfillmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ['account', 'api_id', 'order_api_id', 'tracking_url', 'tracking_number', 'tracking_company','status','last_sync'];
        Schema::create('fulfillments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account', 20);
            $table->string('api_id', 20);
            $table->string('order_api_id', 20);
            $table->string('tracking_url');
            $table->string('tracking_number', 50);
            $table->string('tracking_company', 100);            
            $table->string('status', 20);
            $table->string('last_sync', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fulfillments');
    }
}
