<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->string('account', 10);
            $table->string('api_id', 10);
            $table->string('name', 100);
            $table->string('price', 10);
            $table->string('variant_api_id', 20);
            $table->string('product_api_id', 20);            
            $table->string('created', 20);
            $table->string('last_sync', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
