@extends('layouts.app')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">List all product variants</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div style="margin-bottom:5px;" class="col-md-12">
                            <label>Search by</label>
                            <select id="search_by" class="form-control"> 
                                <option value="line_items">Line Item (Item name match the pattern in the search box)</option>
                            </select>
                        </div>
                    </div>

                    <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                            Save
                    </div>
                    <div class="btn btn-warning" style="margin-bottom:10px;" onclick="updateTable()">
                            Reset
                    </div>
                    
                   <table id="table" class="table table-striped table-bordered">
                        <thead>
                            <th>Name</th>
                            <th>Variant Id</th>
                            <th>Product Id</th>
                            <th>Number Of Units</th>
                            <th>Number Sold</th>
                            <th>Number Unit Sold</th>
                            <th>Number Left</th>
                            <th>Total Stocks</th>                                                       
                            <th>Grouping</th>                                                       
                        </thead>		                            
                    </table>			                

                    <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                        Save
                    </div>                                     
                    <div class="btn btn-warning" style="margin-bottom:10px;" onclick="updateTable()">
                        Reset
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="groupModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="groupModalTitle"></h4>
        </div>
        <div class="modal-body">
            <p id="groupModalMessage"></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="groupingConfirm()">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="degroupModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="degroupModalTitle"></h4>
          <input type="hidden" id="degroupId">
        </div>
        <div class="modal-body">
            <p id="degroupModalMessage"></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" onclick="degroupingConfirm()">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>

<script>

        var group = [];
        var group_name = [];
        var group_product_ids = [];

        $(document).ready(function () {            
            loadTable();            
        });
        
        function loadTable() {  
            //var search_by = $("#search_by").val();
            $('#table').DataTable({
                    "order": [[ 4, "desc" ]],
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                        "url": "products/read",
                        "dataType": "json",
                        "type": "POST",
                        "data":function (d) {
                            d.search_by = document.getElementById("search_by").value
                        }
                    },
                    "columns": [
                        { "data": "name" },
                        { "data": "variant_id" },
                        { "data": "product_id" },
                        { "data": "number_of_units" },
                        { "data": "number_sold" },
                        { "data": "number_unit_sold" },
                        { "data": "number_left" },                        
                        { "data": "option" },
                        { "data": "grouping" }
                    ]	 
                });
            
            }

        function checkbox(element){

            if(element.checked){
                $variant_id = $(element).siblings('[name = "grouping_variant_id"]').val();
                $variant_name = $(element).siblings('[name = "grouping_variant_name"]').val();
                $product_id = $(element).siblings('[name = "grouping_product_id"]').val();

                // if((group_product_id != null) && (group_product_id != $product_id)){
                //     alert("You must choose the variant from the same product to group");
                //     element.checked = false;
                //     return;
                // }

                $(element).siblings('[name = "group_button"]').attr("style", "");                  
                group_product_ids.push($product_id);
                group.push($variant_id);                
                group_name.push($variant_name);                


            }else{
                $(element).siblings('[name = "group_button"]').attr("style", "display:none;");                
                $variant_id = $(element).siblings('[name = "grouping_variant_id"]').val();
                for (i = 0; i < group.length; i++) { 
                    if(group[i] == $variant_id){
                        group.splice(i, 1);
                        group_name.splice(i, 1);
                        group_product_ids.splice(i, 1);
                        return;
                    }
                }                
            }
        }

        function groupVariant(){
            if(group.length < 2){
                alert("You must select at least 2 product variant to group");
                return;
            }

            var message = "";
            for(i = 0; i < group_name.length; i++){
                message += "<br>" + group_name[i];
            }

            $("#groupModalTitle").html('Do you want to group the following product variants?');
            $("#groupModalMessage").html(message);
            $("#groupModal").modal('toggle');
        }

        function degroupVariant(element){

            $variant_id = $(element).siblings('[name = "grouping_variant_id"]').val();
            $variant_name = $(element).siblings('[name = "grouping_variant_name"]').val();                
            
            $("#degroupModalTitle").html('Do you want to degroup the following product variants?');
            $("#degroupId").val($variant_id);            
            $("#degroupModalMessage").html($variant_name);
            $("#degroupModal").modal('toggle');
        }

        function degroupingConfirm(){
            
            $variants_id = $("#degroupId").val();            
            $.ajax({                    
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: 'products/degroupVariant',
                        data: {
                            'variants_id'             : $variants_id
                        },
                        method: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            alert("You have degrouped successfully the variants"); 
                            $("#degroupModal").modal('hide');                                   
                            updateTable();                                                                
                        },error: function(jqXHR,error,errorThrown){
                            alert(JSON.stringify(jqXHR));                                    
                        }
                    });     

        }

        function groupingConfirm(){
            
            $.ajax({                    
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: 'products/groupVariant',
                        data: {
                            'group'             : group
                        },
                        method: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            alert("You have grouped successfully the variants");   
                            $("#groupModal").modal('hide');                                                                    
                            updateTable();                                                                
                        },error: function(jqXHR,error,errorThrown){
                            alert(JSON.stringify(jqXHR));                                    
                        }
                    });     

        }

        function saveTracking(){
            $( ".tracking" ).each(function(index) {
            
                variant_id = $(this).children('[name = "variant_id"]').val();                
                total = $(this).children('[name = "total"]').val();

                var $message = $(this).children('[name = "message"]');

                if(total != ''){
                    $.ajax({                    
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: 'products/edit',
                        data: {
                            'variant_id'             : variant_id,
                            'total'                  : total
                        },
                        method: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            $message.html("You have edited successfully the variant");        
                            $message.attr("class", "alert alert-success");     
                            updateTable();                                                                
                        },error: function(jqXHR,error,errorThrown){
                            alert(JSON.stringify(jqXHR));                                    
                        }
                    });         
                }else{
                    $message.html("Invalid params");        
                    $message.attr("class", "alert alert-danger");                                                                        
                }
            });        

            $( ".number_of_units" ).each(function(index) {
            
                variant_id = $(this).children('[name = "variant_id"]').val();                
                number_of_units = $(this).children('[name = "number_of_units"]').val();

                var $message = $(this).children('[name = "message"]');

                if(number_of_units != '' && number_of_units >= 1){
                    $.ajax({                    
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: 'products/editNumberOfUnits',
                        data: {
                            'variant_id'             : variant_id,
                            'number_of_units'        : number_of_units
                        },
                        method: 'POST',
                        dataType: 'JSON',
                        success: function (response) {
                            $message.html("You have edited successfully the variant");        
                            $message.attr("class", "alert alert-success");     
                            updateTable();                                                                
                        },error: function(jqXHR,error,errorThrown){
                            alert(JSON.stringify(jqXHR));                                    
                        }
                    });         
                }else{
                    $message.html("Invalid params");        
                    $message.attr("class", "alert alert-danger");                                                                        
                }
            });         

        }

        $("#search_by").change(function(){
            updateTable();
        });

        function updateTable(){

            table = $("#table").DataTable();
            table.ajax.reload(null, false);
            group = [];
            group_name = [];
            group_product_ids = [];
        };

        
    </script>
        
@endsection
