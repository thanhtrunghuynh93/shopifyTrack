@extends('layouts.app')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">List all orders</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <!-- <div style="margin-bottom:5px;" class="col-md-6">
                            <label>Filter by</label>
                            <select id="filter_by" class="form-control"> 
                                <option value="none">None</option>                        
                                <option value="fulfilled">Fulfilled orders</option>
                                <option value="unfulfilled">Unfulfilled orders</option>        
                            </select>
                        </div> -->
                        <div style="margin-bottom:5px;" class="col-md-12">
                            <label>Search by</label>
                            <select id="search_by" class="form-control"> 
                                <option value="line_items">Line Item (Item name match the pattern in the search box)</option>
                                <option value="order_number">Order Number (Filter order number by the range below) and Line Item</option>
                                <!-- <option value="customer_name">Customer name (Customer name match the pattern in the search box)</option>                             -->
                            </select>
                        </div>
                        <div style="margin-bottom:5px; display:none;" class="col-md-12 form-inline" id="order_number_options">
                            <label>Prefix:</label>
                            <select id="order_number_prefix" class="form-control">
                                <option value="#">#</option>
                                <option value="#SO">#SO</option>
                            </select>
                            <label>From:</label><input id="order_number_from" class="form-control" type="number" value="7000">                
                            <label>To:</label><input id="order_number_to" class="form-control" type="number" value="8000">                                            
                        </div>
                    </div>
                                            
                    @if(Auth::user()->role == 'admin')
                        <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                                Save Tracking Informations
                        </div>
                    @endif
                    @if(Auth::user()->role == 'trackingFiller')
                        <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                                Save Temporary Tracking Informations
                        </div>
                    @endif
                    <div class="btn btn-success save-tracking" style="margin-bottom:10px;" onclick="trackingUrlModal()">
                            Edit Tracking Url Options
                    </div>
                    
                   <table id="table" class="table table-striped table-bordered">
                        <thead>
                            <th>Name</th>
                            <th>Customer</th>
                            <!-- <th>Line item</th> -->
                            <th>Fulfillment Status</th>
                            <th>Total price</th>
                            <th>Payment Gate</th>                            
                            <th>Item [Track number][Track url][Track company]</th>
                        </thead>		                            
                    </table>			                

                    @if(Auth::user()->role == 'admin')
                        <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                                Save Tracking Informations
                        </div>
                    @endif
                    @if(Auth::user()->role == 'trackingFiller')
                        <div class="btn btn-primary save-tracking" style="margin-bottom:10px;" onclick="saveTracking()">
                                Save Temporary Tracking Informations
                        </div>
                    @endif
                    <div class="btn btn-success save-tracking" style="margin-bottom:10px;" onclick="trackingUrlModal()">
                        Edit Tracking Url Options
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="trackingUrlModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">List tracking url options</h4>
        </div>
        <div class="modal-body">
          <label>Tracking url options (Each url in each line)</label>
          <textarea rows="20" class="form-control" id="tracking_urls">
          </textarea>          
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveTrackingUrl()">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- <div class="modal fade" id="createModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Do you want to create this fulfillment?</h4>
        </div>
        <div class="modal-body">
            <label>Tracking number</label>
            <input id="create_tracking_number" class="form-control" type="text">
            <label>Tracking url</label>
            <input id="create_tracking_url" class="form-control" type="text">
            <label>Tracking company</label>
            <input id="create_tracking_company" class="form-control" type="text" value="Other">
            <input id="create_order_id" type="hidden">          
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="create_submit">OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>

<script>
        $(document).ready(function () {            
            loadTable();

            $("#search_by").change(function(){
                if($("#search_by").val() == "order_number"){
                    $("#order_number_options").attr("style","margin-bottom:5px;");
                }else{
                    $("#order_number_options").attr("style","margin-bottom:5px; display:none;");
                }                
            });
        });
        
        function loadTable() {  
            //var search_by = $("#search_by").val();
            $('#table').DataTable({
                    "order": [[ 0, "desc" ]],
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                        "url": "orders/read",
                        "dataType": "json",
                        "type": "POST",
                        "data":function (d) {
                            d.search_by = document.getElementById("search_by").value
                            d.order_number_prefix = document.getElementById("order_number_prefix").value
                            d.order_number_from = document.getElementById("order_number_from").value
                            d.order_number_to = document.getElementById("order_number_to").value
                        }
                    },
                    "columns": [
                        { "data": "name" },
                        { "data": "customer" },
                        // { "data": "line_items" },
                        { "data": "fulfillment" },
                        { "data": "total_price" },
                        { "data": "gateway" },
                        { "data": "option" }
                    ]	 
                });
            
            }


        // function createFulfillment(id){            
        //     $("#create_order_id").val(id);                
        //     $("#createModal").modal('toggle');
        // }

        function saveTracking(){
            var invalid = [];
            
            $( ".tracking" ).each(function(index) {
            
                order_id = $(this).children('[name = "order_id"]').val();
                order_name = $(this).children('[name = "order_name"]').val();
                tracking_number = $(this).children('[name = "tracking_number"]').val();
                tracking_url = $(this).children('[name = "tracking_url"]').val();
                tracking_company = $(this).children('[name = "tracking_company"]').val();
                fulfillment_id = $(this).children('[name = "fulfillment_id"]').val();
                fulfillment_api_id = $(this).children('[name = "fulfillment_api_id"]').val();
                item_id = $(this).children('[name = "item_id"]').val();

                var $message = $(this).children('[name = "message"]');

                if(tracking_number != '' && tracking_url != '' && tracking_company != ''){
                 
                    if(fulfillment_id == undefined){    
                        $.ajax({                    
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: 'fulfillment/create',
                            data: {
                                'order_id'               : order_id,
                                'item_id'                : item_id,
                                'tracking_number'        : tracking_number,
                                'tracking_url'           : tracking_url,
                                'tracking_company'       : tracking_company,
                            },
                            method: 'POST',
                            dataType: 'JSON',
                            success: function (response) {                            
                                $message.html("You have created successfully the tracking");        
                                $message.attr("class", "alert alert-success");   
                                updateTable(); 

                            },error: function(jqXHR,error,errorThrown){
                                alert(JSON.stringify(jqXHR));                        
                            }
                        });
                    }else{                        
                        $.ajax({                    
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: 'fulfillment/edit',
                                data: {
                                    'order_id'               : order_id,
                                    'item_id'                : item_id,
                                    'fulfillment_id'         : fulfillment_id,
                                    'fulfillment_api_id'     : fulfillment_api_id,
                                    'tracking_number'        : tracking_number,
                                    'tracking_url'           : tracking_url,
                                    'tracking_company'       : tracking_company,
                                },
                                method: 'POST',
                                dataType: 'JSON',
                                success: function (response) {
                                    $message.html("You have edited successfully the tracking");        
                                    $message.attr("class", "alert alert-success");    
                                    updateTable();                                                                    
                                },error: function(jqXHR,error,errorThrown){
                                    alert(JSON.stringify(jqXHR));                                    
                                }
                        });         
                    }
                }else{
                    $message.html("Invalid params");        
                    $message.attr("class", "alert alert-danger");                                                                        
                }
            });            
        }

        

        function trackingUrlModal(){                    
            $.ajax({                    
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'fulfillment/tracking_urls',
                    method: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        var data = response.data;  
                        $("#tracking_urls").val(data);
                        $("#trackingUrlModal").modal('toggle');
                    },error: function(jqXHR,error,errorThrown){
                        alert(jqXHR.responseJSON.message);                        
                    }
            });         
        }

        function saveTrackingUrl(){             

            tracking_urls = $("#tracking_urls").val();

            $.ajax({                    
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'fulfillment/save_tracking_urls',
                    data: {
                        'tracking_urls'     : tracking_urls,                                
                    },
                    method: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        alert(response.message);
                        $("#trackingUrlModal").modal('hide');
                    },error: function(jqXHR,error,errorThrown){
                        alert(jqXHR.responseJSON.message);                        
                    }
            });         
        }

        // $("#create_submit").click(function(){

        //     var tracking_number = $("#create_tracking_number").val();
        //     var tracking_url = $("#create_tracking_url").val();
        //     var tracking_company = $("#create_tracking_company").val();
        //     var order_id = $("#create_order_id").val();

        //     if(tracking_number === '' || tracking_url === '' || tracking_company === ''){
        //         alert("Missing information, must fill in tracking number, tracking url, tracking company");
        //         return;
        //     }

        //     $.ajax({                    
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             url: 'fulfillment/create',
        //             data: {
        //                 'order_id'              : order_id,
        //                 'tracking_number'       : tracking_number,
        //                 'tracking_url'          : tracking_url,
        //                 'tracking_company'      : tracking_company,
        //             },
        //             method: 'POST',
        //             dataType: 'JSON',
        //             success: function (response) {
        //                 alert(response.message);
        //                 $("#createModal").modal('hide');                                    
        //                 updateTable();
        //             },error: function(jqXHR,error,errorThrown){
        //                 alert(JSON.stringify(jqXHR));
        //                 $("#createModal").modal('hide');       
        //             }
        //     });         
        // });

        // $("#edit_submit").click(function(){

        //     var fulfillment_id = $("#edit_fulfillment_id").val();
        //     var tracking_number = $("#edit_tracking_number").val();
        //     var tracking_url = $("#edit_tracking_url").val();
        //     var tracking_company = $("#edit_tracking_company").val();
        //     var order_id = $("#edit_order_id").val();

        //     alert(fulfillment_id);
        //     alert(order_id);

        //     if(tracking_number === '' || tracking_url === '' || tracking_company === ''){
        //         alert("Missing information, must fill in tracking number, tracking url, tracking company");
        //         return;
        //     }

        //     $.ajax({                    
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             url: 'fulfillment/edit',
        //             data: {
        //                 'order_id'              : order_id,
        //                 'fulfillment_id'        : fulfillment_id,
        //                 'tracking_number'       : tracking_number,
        //                 'tracking_url'          : tracking_url,
        //                 'tracking_company'      : tracking_company,
        //             },
        //             method: 'POST',
        //             dataType: 'JSON',
        //             success: function (response) {
        //                 alert(response.message);
        //                 $("#createModal").modal('hide');                                    
        //                 updateTable();
        //             },error: function(jqXHR,error,errorThrown){
        //                 alert(JSON.stringify(jqXHR));
        //                 $("#createModal").modal('hide');       
        //             }
        //     });         
        // });
        
        $("#search_by").change(function(){
            updateTable();
        });

        $("#order_number_prefix").change(function(){
            updateTable();
        });

        $("#order_number_from").change(function(){
            updateTable();
        });

        $("#order_number_to").change(function(){
            updateTable();
        });

        function updateTable(){

            table = $("#table").DataTable();
            table.ajax.reload(null, false);
        };

        
    </script>
        
@endsection
