@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Choose an account</div>
                <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif                   
                            <div class="form-group">
                                <select class="form-control" name="account" id="account">
                                    @foreach($accounts as $account)                                        
                                        <option value="{{$account['id']}}">{{$account['shop_url']}}</option>
                                    @endforeach                
                                </select>
                            </div>
                            <button id="select" class="btn btn-primary">Select</button>                    					                    
                        
                </div>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script>
        $(document).ready(function () {            
            $("#select").click(function(){                
                window.location.href = 'account/select/' + $('#account').val();
            });
        });               
</script>
        
@endsection
