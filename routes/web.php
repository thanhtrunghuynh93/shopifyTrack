<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'account', 'middleware' => 'auth'], function () {
    Route::get('', 'AccountController@index');
    Route::get('select/{account_id}', 'AccountController@select');
});

Route::group(['prefix' => 'orders', 'middleware' => 'auth'], function () {
    Route::get('', 'OrderController@index');
    Route::post('read', 'OrderController@read');        
    Route::get('fetch/{order_id}', 'OrderController@fetch');
});

Route::group(['prefix' => 'products', 'middleware' => 'auth'], function () {
    Route::get('', 'ProductController@index');
    Route::post('read', 'ProductController@read');        
    Route::post('edit', 'ProductController@edit');        
    Route::post('editNumberOfUnits', 'ProductController@editNumberOfUnits');            
    Route::post('groupVariant', 'ProductController@groupVariant');            
    Route::post('degroupVariant', 'ProductController@degroupVariant');            
});

Route::group(['prefix' => 'fulfillment', 'middleware' => 'auth'], function () {
    Route::get('tracking_urls', 'FulfillmentController@trackingUrls');
    Route::post('save_tracking_urls', 'FulfillmentController@saveTrackingUrls');
    Route::post('create', 'FulfillmentController@create');
    Route::post('read', 'FulfillmentController@read');        
    Route::post('edit', 'FulfillmentController@edit');        
});



